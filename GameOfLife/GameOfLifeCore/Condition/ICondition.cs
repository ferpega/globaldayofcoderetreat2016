﻿using GameOfLife.Core.Universe;
using System.Collections.Generic;

namespace GameOfLife.Core.Condition
{
    public interface ICondition
    {
        IEnumerable<Cell> GetCellsMatchingCondition(UniverseGrid universe);
    }
}