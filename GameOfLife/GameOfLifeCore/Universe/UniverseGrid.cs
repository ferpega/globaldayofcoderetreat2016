﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GameOfLife.Core.Universe
{
    public class UniverseGrid
    {
        private Cell[,] _cells;
        private readonly int _verSize;
        private readonly int _horSize;

        public UniverseGrid(int horizontalSize, int verticalSize)
        {
            if (horizontalSize < 3 || verticalSize < 3)
                throw new IndexOutOfRangeException("Universe must be of 3x3 Grid at minimum");

            _horSize = horizontalSize;
            _verSize = verticalSize;
            _cells = new Cell[horizontalSize, verticalSize];
            for (int x = 0; x < _cells.GetLength(0); x++)
            {
                for (int y = 0; y < _cells.GetLength(1); y++)
                {
                    _cells[x, y] = new Cell();
                }
            }
        }

        public int HorizontalSize
        {
            get { return _horSize; }
        }

        public int VerticalSize
        {
            get { return _verSize; }
        }

        public Cell this[int inColumn, int inRow]
        {
            get
            {
                inColumn--;
                inRow--;

                if (0 > inColumn || inColumn >= HorizontalSize) return null;
                if (0 > inRow || inRow >= HorizontalSize) return null;

                return _cells[inColumn, inRow];
            }
        }

        internal IEnumerable<Cell> GetCellsInUniverseWithConditions(Func<Cell, int, bool> conditionCell)
        {
            IList<Cell> result = new List<Cell>();
            for (int h = 1; h <= _cells.GetLength(0); h++)
                for (int v = 1; v <= _cells.GetLength(1); v++)
                {
                    if (conditionCell(this[h, v], this.GetAliveNeighboursOf(h, v).Count())) result.Add(this[h, v]);
                }
            return result;
        }

        private IEnumerable<Cell> GetNeighbours(int inColumn, int inRow)
        {
            IList<Cell> result = new List<Cell>();

            var previousRow = inRow - 1;
            var nextRow = inRow + 1;
            var previousColumn = inColumn - 1;
            var nextColumn = inColumn + 1;

            AddToCollectionIfNotNull(result, this[previousColumn, previousRow]);
            AddToCollectionIfNotNull(result, this[inColumn, previousRow]);
            AddToCollectionIfNotNull(result, this[nextColumn, previousRow]);

            AddToCollectionIfNotNull(result, this[previousColumn, inRow]);
            AddToCollectionIfNotNull(result, this[nextColumn, inRow]);

            AddToCollectionIfNotNull(result, this[previousColumn, nextRow]);
            AddToCollectionIfNotNull(result, this[inColumn, nextRow]);
            AddToCollectionIfNotNull(result, this[nextColumn, nextRow]);

            return result;
        }

        public IEnumerable<Cell> GetAliveNeighboursOf(int inColumn, int inRow)
        {
            var livingNeighbors = from n in this.GetNeighbours(inColumn, inRow)
                                  where n.IsAlive
                                  select n;
            return livingNeighbors;
        }

        public void SetUniverseStatus(string[] map)
        {
            if (map.Count() != this.HorizontalSize)
                throw new IndexOutOfRangeException("Vertical size is different than map vertical size.");
            if (map[0].Length != this.VerticalSize)
                throw new IndexOutOfRangeException("Horizontal size is different than map horizontal size.");

            for (int h = 0; h < map[0].Length; h++)
            {
                for (int v = 0; v < map.Count(); v++)
                {
                    this[h + 1, v + 1].IsAlive = map[v].Substring(h, 1) != ".";
                }
            }
        }

        private void AddToCollectionIfNotNull(IList<Cell> resultNeighbors, Cell cell)
        {
            if (cell != null) resultNeighbors.Add(cell);
        }

    }
}

