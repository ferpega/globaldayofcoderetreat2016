﻿using FluentAssertions;
using NUnit.Framework;

namespace GameOfLife.Core.Tests.Specs
{
    public class CellTests
    {
        [Test]
        public void Cell_DefaultConstructor()
        {
            var cell = new Cell();
            cell.IsAlive.Should().BeFalse();
        }

        [Test]
        public void Cell_ConstructorWithCellAlive()
        {
            var cell = new Cell(true);
            cell.IsAlive.Should().BeTrue();
        }

        [Test]
        public void Cell_ConstructorWithDeadCell()
        {
            var cell = new Cell(false);
            cell.IsAlive.Should().BeFalse();
        }
    }
}