﻿using FluentAssertions;
using GameOfLife.Core.Condition;
using GameOfLife.Core.Universe;
using NUnit.Framework;
using System.Linq;

namespace GameOfLife.Core.Tests.Specs.Condition
{
    class ConditionOvercrowdingTests
    {
        [Test]
        public void CheckCells_With_Overcrowding_Neighbours()
        {
            var universe = new UniverseGrid(3, 3);
            universe[1, 1].IsAlive = true;
            universe[2, 1].IsAlive = true;
            universe[1, 2].IsAlive = true;
            universe[2, 2].IsAlive = true;
            universe[3, 3].IsAlive = true;

            var condition = new ConditionOvercrowding();
            var resultCells = condition.GetCellsMatchingCondition(universe);
            resultCells.Count().Should().Be(1);
        }
    }
}
