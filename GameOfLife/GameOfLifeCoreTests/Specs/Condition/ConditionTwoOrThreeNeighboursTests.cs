﻿using FluentAssertions;
using GameOfLife.Core.Condition;
using GameOfLife.Core.Universe;
using NUnit.Framework;
using System.Linq;

namespace GameOfLife.Core.Tests.Specs.Condition
{
    class ConditionTwoOrThreeNeighboursTests
    {
        [Test]
        public void CheckCells_With_Two_Or_Three_Neighbours()
        {
            var universe = new UniverseGrid(3, 3);
            universe[1, 1].IsAlive = true;
            universe[2, 2].IsAlive = true;
            universe[3, 3].IsAlive = true;

            var condition = new ConditionTwoOrThreeNeighbours();
            var resultCells = condition.GetCellsMatchingCondition(universe);
            resultCells.Count().Should().Be(1);
        }
    }
}
