﻿using FluentAssertions;
using GameOfLife.Core.Condition;
using GameOfLife.Core.Universe;
using NUnit.Framework;
using System.Linq;

namespace GameOfLife.Core.Tests.Specs.Condition
{
    class ConditionUnderpopulationTests
    {
        [Test]
        public void CheckCells_In_Underpopulation_Status_None()
        {
            var universe = new UniverseGrid(3, 3);
            // Row 1
            universe[1, 2].IsAlive = true;
            // Row 2
            universe[2, 1].IsAlive = true;
            universe[2, 2].IsAlive = true;
            universe[2, 3].IsAlive = true;
            // Row 3
            universe[2, 3].IsAlive = true;

            var condition = new ConditionUnderpopulation();
            var resultCells = condition.GetCellsMatchingCondition(universe);

            resultCells.Count().Should().Be(0);
        }

        [Test]
        public void CheckCells_In_Underpopulation_Status_NoOneAlive()
        {
            var universe = new UniverseGrid(3, 3);

            var condition = new ConditionUnderpopulation();
            var resultCells = condition.GetCellsMatchingCondition(universe);

            resultCells.Count().Should().Be(0);
        }

        [Test]
        public void CheckCentralCell_Is_Not_Underpopulation_ButTheOthersYes()
        {
            var universe = new UniverseGrid(3, 3);
            universe[1, 1].IsAlive = true;
            universe[2, 2].IsAlive = true;
            universe[3, 3].IsAlive = true;

            var condition = new ConditionUnderpopulation();
            var resultCells = condition.GetCellsMatchingCondition(universe);

            resultCells.Count().Should().Be(2);
        }
    }
}
